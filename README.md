<a name="readme-top"></a>

[![Last Version][tag-shield]][tag-url]
[![Contributors][contributors-shield]][contributors-url]
[![Last Commit][last-commit-shield]][last-commit-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![CC-BY-SA 4.0][license-shield]][license-url]
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=for-the-badge)](https://github.com/prettier/prettier)

- [![Cypress][Cypress-badge]][Cypress-url]
- [![Novu][Novu-badge]][Novu-url]
- [![Quasar][Quasar-badge]][Quasar-url]
- [![Storybook][Storybook-badge]][Storybook-url]
- [![Supabase][Supabase-badge]][Supabase-url]
- [![Vite][Vite-badge]][Vite-url]
- [![Vitest][Vitest-badge]][Vitest-url]
- [![Vue][Vue-badge]][Vue-url]

# Quasar Supabase Novu (quasar-supabase-novu)

A Quasar Project

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

[contributors-shield]: https://img.shields.io/gitlab/contributors/46135783?style=for-the-badge
[contributors-url]: https://gitlab.com/umbra-cultura/quasar-supabase-novu/-/project_members
[tag-shield]: https://img.shields.io/gitlab/v/tag/46135783?style=for-the-badge
[tag-url]: #
[last-commit-shield]: https://img.shields.io/gitlab/last-commit/46135783?style=for-the-badge
[last-commit-url]: https://gitlab.com/umbra-cultura/quasar-supabase-novu
[forks-shield]: https://img.shields.io/gitlab/forks/46135783?style=for-the-badge
[forks-url]: https://gitlab.com/umbra-cultura/quasar-supabase-novu/-/forks
[stars-shield]: https://img.shields.io/gitlab/stars/46135783?style=for-the-badge
[stars-url]: https://gitlab.com/umbra-cultura/quasar-supabase-novu/-/starrers
[issues-shield]: https://img.shields.io/gitlab/issues/open/46135783?style=for-the-badge
[issues-url]: https://gitlab.com/umbra-cultura/quasar-supabase-novu/-/issues
[license-shield]: https://img.shields.io/gitlab/license/46135783?style=for-the-badge
[license-url]: https://gitlab.com/umbra-cultura/quasar-supabase-novu/-/blob/main/LICENSE.md
[Cypress-badge]: https://img.shields.io/badge/Cypress-F0FCF8?style=for-the-badge&logo=cypress&logoColor=black
[Cypress-url]: https://www.cypress.io/
[Novu-badge]: https://img.shields.io/badge/Novu-262626?style=for-the-badge&logo=novu&logoColor=ff006a
[Novu-url]: https://novu.co
[Quasar-badge]: https://img.shields.io/badge/quasar-1976d2?style=for-the-badge&logo=quasar&logoColor=white
[Quasar-url]: https://quasar.dev
[Supabase-badge]: https://img.shields.io/badge/Supabase-3FCF8E?style=for-the-badge&logo=supabase&logoColor=white
[Supabase-url]: https://supabase.com/
[Storybook-badge]: https://img.shields.io/badge/Storybook-FF4785?style=for-the-badge&logo=storybook&logoColor=white
[Storybook-url]: https://storybook.js.org/
[Vite-badge]: https://img.shields.io/badge/Vite-747BFF?style=for-the-badge&logo=vite&logoColor=white
[Vite-url]: https://vitejs.dev/
[Vitest-badge]: https://img.shields.io/badge/Vitest-6DA13F?style=for-the-badge&logo=vitest&logoColor=white
[Vitest-url]: https://vitest.dev/
[Vue-badge]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
