/** @type { import('@storybook/vue3').Preview } */

import "@quasar/extras/roboto-font/roboto-font.css";
// These are optional
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/animate/fadeInUp.css";
import "@quasar/extras/animate/fadeOutDown.css";
import "@quasar/extras/animate/fadeInRight.css";
import "@quasar/extras/animate/fadeOutRight.css";

// Loads the quasar styles and registers quasar functionality with storybook
import "quasar/dist/quasar.css";

import DocumentationTemplate from "./DocumentationTemplate.mdx";
import { setup } from "@storybook/vue3";
import { createPinia } from "pinia";
import { Quasar, Notify } from "quasar";

setup((app) => {
  const Store = new createPinia();

  app.use(Store);
  app.use(Quasar, {
    plugins: {
      Notify,
    },
    config: {
      notify: {},
    },
  });
});

const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    docs: {
      page: DocumentationTemplate,
    },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
