import QStepper from "quasar";

export default {
  title: "Quasar/Stepper",
  component: QStepper,
  tags: ["autodocs"],
  argTypes: {
    "keep-alive": {
      title: "keep-alive",
      type: { name: "boolean", required: false },
      description:
        "Equivalent to using Vue's native <keep-alive> component on the content",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "keep-alive-include": {
      title: "keep-alive-include",
      type: { name: "object", required: false },
      description:
        "Equivalent to using Vue's native include prop for <keep-alive>; Values must be valid Vue component names",
      control: "object",
      table: { category: "Behavior" },
    },
    "keep-alive-exclude": {
      title: "keep-alive-exclude",
      type: { name: "object", required: false },
      description:
        "Equivalent to using Vue's native exclude prop for <keep-alive>; Values must be valid Vue component names",
      control: "object",
      table: { category: "Behavior" },
    },
    "keep-alive-max": {
      title: "keep-alive-max",
      type: { name: "number", required: false },
      description: "Equivalent to using Vue's native max prop for <keep-alive>",
      control: "number",
      table: { category: "Behavior" },
    },
    animated: {
      title: "animated",
      type: { name: "boolean", required: false },
      description:
        "Enable transitions between panel (also see 'transition-prev' and 'transition-next' props)",
      control: "boolean",
      table: { category: "Behavior" },
    },
    infinite: {
      title: "infinite",
      type: { name: "boolean", required: false },
      description:
        "Makes component appear as infinite (when reaching last panel, next one will become the first one)",
      control: "boolean",
      table: { category: "Behavior" },
    },
    swipeable: {
      title: "swipeable",
      type: { name: "boolean", required: false },
      description:
        "Enable swipe events (may interfere with content's touch/mouse events)",
      control: "boolean",
      table: { category: "Behavior" },
    },
    vertical: {
      title: "vertical",
      type: { name: "boolean", required: false },
      description:
        "Put Stepper in vertical mode (instead of horizontal by default)",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "header-nav": {
      title: "header-nav",
      type: { name: "boolean", required: false },
      description: "Allow navigation through the header",
      control: "boolean",
      table: { category: "Behavior" },
    },
    contracted: {
      title: "contracted",
      type: { name: "boolean", required: false },
      description: "Hide header labels on narrow windows",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "alternative-labels": {
      title: "alternative-labels",
      type: { name: "boolean", required: false },
      description:
        "Use alternative labels - stacks the icon on top of the label (applies only to horizontal stepper)",
      control: "boolean",
      table: { category: "Header" },
    },
    "inactive-icon": {
      title: "inactive-icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Header" },
    },
    "inactive-color": {
      title: "inactive-color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Header" },
    },
    "done-icon": {
      title: "done-icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; If 'none' (String) is used as value, then it will defer to prefix or the regular icon for this state; Make sure you have the icon library installed unless you are using 'img:' prefix",
      control: "text",
      table: { category: "Header" },
    },
    "done-color": {
      title: "done-color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Header" },
    },
    "active-icon": {
      title: "active-icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; If 'none' (String) is used as value, then it will defer to prefix or the regular icon for this state; Make sure you have the icon library installed unless you are using 'img:' prefix",
      control: "text",
      table: { category: "Header" },
    },
    "active-color": {
      title: "active-color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Header" },
    },
    "error-icon": {
      title: "error-icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; If 'none' (String) is used as value, then it will defer to prefix or the regular icon for this state; Make sure you have the icon library installed unless you are using 'img:' prefix",
      control: "text",
      table: { category: "Header" },
    },
    "error-color": {
      title: "error-color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Header" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "number", required: false },
      description:
        "Model of the component defining the current panel's name; If a Number is used, it does not define the panel's index, but rather the panel's name which can also be an Integer; Either use this property (along with a listener for 'update:model-value' event) OR use the v-model directive.",
      control: "number",
      table: { category: "Model" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    flat: {
      title: "flat",
      type: { name: "boolean", required: false },
      description: "Applies a 'flat' design (no default shadow)",
      control: "boolean",
      table: { category: "Style" },
    },
    bordered: {
      title: "bordered",
      type: { name: "boolean", required: false },
      description: "Applies a default border to the component",
      control: "boolean",
      table: { category: "Style" },
    },
    "header-class": {
      title: "header-class",
      type: { name: "string", required: false },
      description: "Class definitions to be attributed to the header",
      control: "text",
      table: { category: "Style" },
    },
    "transition-prev": {
      title: "transition-prev",
      type: { name: "string", required: false },
      description:
        "One of Quasar's embedded transitions (has effect only if 'animated' prop is set)",
      defaultValue: "slide-right/slide-down",
      control: "text",
      table: { category: "Transition" },
    },
    "transition-next": {
      title: "transition-next",
      type: { name: "string", required: false },
      description:
        "One of Quasar's embedded transitions (has effect only if 'animated' prop is set)",
      defaultValue: "slide-left/slide-up",
      control: "text",
      table: { category: "Transition" },
    },
    "transition-duration": {
      title: "transition-duration",
      type: { name: "number", required: false },
      description: "Transition duration (in milliseconds, without unit)",
      defaultValue: 300,
      control: "number",
      table: { category: "Transition" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "Steppers display progress through a sequence of logical and numbered steps. They may also be used for navigation. They’re usually useful when the user has to follow steps to complete a process, like in a wizard.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QStepper },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template:
    '<q-stepper v-bind="args"><q-step :name="1" title="Select campaign settings" icon="settings" :done="step > 1">For each ad campaign that you create, you can control how much youre willing to spend on clicks and conversions, which networks and geographical locations you want your ads to show on, and more.</q-step><q-step :name="2" title="Select campaign settings" icon="settings" :done="step > 2">For each ad campaign that you create, you can control how much youre willing to spend on clicks and conversions, which networks and geographical locations you want your ads to show on, and more.</q-step></q-stepper>',
});
export const Standard = Template.bind({});
Standard.args = {
  "model-value": 1,
  ref: "stepper",
  color: "primary",
  animated: true,
};
