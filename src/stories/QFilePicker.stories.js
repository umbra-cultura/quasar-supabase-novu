import QFilePicker from "quasar";

export default {
  title: "Quasar/File Picker",
  component: QFilePicker,
  tags: ["autodocs"],
  argTypes: {
    name: {
      title: "name",
      type: { name: "string", required: false },
      description:
        "Used to specify the name of the control; Useful if dealing with forms; If not specified, it takes the value of 'for' prop, if it exists",
      control: "text",
      table: { category: "Behavior" },
    },
    multiple: {
      title: "multiple",
      type: { name: "boolean", required: false },
      description: "Allow multiple file uploads",
      control: "boolean",
      table: { category: "Behavior" },
    },
    accept: {
      title: "accept",
      type: { name: "string", required: false },
      description:
        "Comma separated list of unique file type specifiers. Maps to 'accept' attribute of native input type=file element",
      control: "text",
      table: { category: "Behavior" },
    },
    capture: {
      title: "capture",
      type: { name: "string", required: false },
      description:
        "Optionally, specify that a new file should be captured, and which device should be used to capture that new media of a type defined by the 'accept' prop. Maps to 'capture' attribute of native input type=file element",
      control: "select",
      options: ["user", "environment"],
      table: { category: "Behavior" },
    },
    "max-file-size": {
      title: "max-file-size",
      type: { name: "number", required: false },
      description: "Maximum size of individual file in bytes",
      control: "number",
      table: { category: "Behavior" },
    },
    "max-total-size": {
      title: "max-total-size",
      type: { name: "number", required: false },
      description: "Maximum size of all files combined in bytes",
      control: "number",
      table: { category: "Behavior" },
    },
    "max-files": {
      title: "max-files",
      type: { name: "number", required: false },
      description: "Maximum number of files to contain",
      control: "number",
      table: { category: "Behavior" },
    },
    /*filter: {
    
: (files) => Array
Description
Custom filter for added files; Only files that pass this filter will be added to the queue and uploaded; For best performance, reference it from your scope and do not define it inline
Params
files
: FileList | Array
Description
Candidate files to be added to queue
Return type: Array
Description
Filtered files to be added to queue
Example
:filter="files => files.filter(file => file.size === 1024)"
*/
    error: {
      title: "error",
      type: { name: "boolean", required: false },
      description: "Does field have validation errors?",
      control: "boolean",
      table: { category: "Behavior" },
    },
    rules: {
      title: "rules",
      type: { name: "object", required: false },
      description:
        "Array of Functions/Strings; If String, then it must be a name of one of the embedded validation rules",
      control: "object",
      table: { category: "Behavior" },
    },
    "reactive-rules": {
      title: "reactive-rules",
      type: { name: "boolean", required: false },
      description:
        "By default a change in the rules does not trigger a new validation until the model changes; If set to true then a change in the rules will trigger a validation; Has a performance penalty, so use it only when you really need it",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "lazy-rules": {
      title: "lazy-rules",
      type: { name: "boolean", required: false },
      description:
        "If set to boolean true then it checks validation status against the 'rules' only after field loses focus for first time; If set to 'ondemand' then it will trigger only when component's validate() method is manually called or when the wrapper QForm submits itself",
      control: "boolean",
      table: { category: "Behavior" },
    },
    loading: {
      title: "loading",
      type: { name: "boolean", required: false },
      description:
        "Signals the user a process is in progress by displaying a spinner; Spinner can be customized by using the 'loading' slot.",
      control: "boolean",
      table: { category: "Behavior" },
    },
    clearable: {
      title: "clearable",
      type: { name: "boolean", required: false },
      description:
        "Appends clearable icon when a value (not undefined or null) is set; When clicked, model becomes null",
      control: "boolean",
      table: { category: "Behavior" },
    },
    autofocus: {
      title: "autofocus",
      type: { name: "boolean", required: false },
      description: "Focus field on initial component render",
      control: "boolean",
      table: { category: "Behavior" },
    },
    for: {
      title: "for",
      type: { name: "string", required: false },
      description:
        "Used to specify the 'id' of the control and also the 'for' attribute of the label that wraps it; If no 'name' prop is specified, then it is used for this attribute as well",
      control: "text",
      table: { category: "Behavior" },
    },
    append: {
      title: "append",
      type: { name: "boolean", required: false },
      description:
        "Append file(s) to current model rather than replacing them; Has effect only when using 'multiple' mode",
      control: "boolean",
      table: { category: "Behavior" },
    },
    /*
    'counter-label': {

: (props) => String
Description
Label for the counter; The 'counter' prop is necessary to enable this one
*/
    "error-message": {
      title: "error-message",
      type: { name: "string", required: false },
      description:
        "Validation error message (gets displayed only if 'error' is set to 'true')",
      control: "text",
      table: { category: "Content" },
    },
    "no-error-icon": {
      title: "no-error-icon",
      type: { name: "boolean", required: false },
      description: "Hide error icon when there is an error",
      control: "boolean",
      table: { category: "Content" },
    },
    label: {
      title: "label",
      type: { name: "string", required: false },
      description:
        "A text label that will “float” up above the input field, once the field gets focus",
      control: "text",
      table: { category: "Content" },
    },
    "stack-label": {
      title: "stack-label",
      type: { name: "boolean", required: false },
      description:
        "Label will be always shown above the field regardless of field content (if any)",
      control: "boolean",
      table: { category: "Content" },
    },
    hint: {
      title: "hint",
      type: { name: "string", required: false },
      description:
        "Helper (hint) text which gets placed below your wrapped form component",
      control: "text",
      table: { category: "Content" },
    },
    "hide-hint": {
      title: "hide-hint",
      type: { name: "boolean", required: false },
      description: "Hide the helper (hint) text when field doesn't have focus",
      control: "boolean",
      table: { category: "Content" },
    },
    prefix: {
      title: "prefix",
      type: { name: "string", required: false },
      description: "Prefix",
      control: "text",
      table: { category: "Content" },
    },
    suffix: {
      title: "suffix",
      type: { name: "string", required: false },
      description: "Suffix",
      control: "text",
      table: { category: "Content" },
    },
    "clear-icon": {
      title: "clear-icon",
      type: { name: "string", required: false },
      description:
        "Custom icon to use for the clear button when using along with 'clearable' prop",
      control: "text",
      table: { category: "Content" },
    },
    "label-slot": {
      title: "label-slot",
      type: { name: "boolean", required: false },
      description:
        "Enables label slot; You need to set it to force use of the 'label' slot if the 'label' prop is not set",
      control: "boolean",
      table: { category: "Content" },
    },
    "bottom-slots": {
      title: "bottom-slots",
      type: { name: "boolean", required: false },
      description: "Enables bottom slots ('error', 'hint', 'counter')",
      control: "boolean",
      table: { category: "Content" },
    },
    counter: {
      title: "counter",
      type: { name: "boolean", required: false },
      description: "Show an automatic counter on bottom right",
      control: "boolean",
      table: { category: "Content" },
    },
    tabindex: {
      title: "tabindex",
      type: { name: "number", required: false },
      description: "Tabindex HTML attribute value",
      control: "number",
      table: { category: "General" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "enum", required: true },
      description:
        "Model of the component; Must be FileList or Array if using 'multiple' prop; Either use this property (along with a listener for 'update:modelValue' event) OR use v-model directive",
      control: "file",
      table: { category: "Model" },
    },
    "display-value": {
      title: "display-value",
      type: { name: "string", required: false },
      description:
        "Override default selection string, if not using 'file' or 'selected' scoped slots and if not using 'use-chips' prop",
      control: "text",
      table: { category: "Selection" },
    },
    "use-chips": {
      title: "use-chips",
      type: { name: "boolean", required: false },
      description: "Use QChip to show picked files",
      control: "boolean",
      table: { category: "Selection" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    readonly: {
      title: "readonly",
      type: { name: "boolean", required: false },
      description: "Put component in readonly mode",
      control: "boolean",
      table: { category: "State" },
    },
    "label-color": {
      title: "label-color",
      type: { name: "string", required: false },
      description:
        "Color name for the label from the Quasar Color Palette; Overrides the 'color' prop; The difference from 'color' prop is that the label will always have this color, even when field is not focused",
      control: "text",
      table: { category: "Style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    "bg-color": {
      title: "bg-color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    filled: {
      title: "filled",
      type: { name: "boolean", required: false },
      description: "Use 'filled' design for the field",
      control: "boolean",
      table: { category: "Style" },
    },
    outlined: {
      title: "outlined",
      type: { name: "boolean", required: false },
      description: "Use 'outlined' design for the field",
      control: "boolean",
      table: { category: "Style" },
    },
    borderless: {
      title: "borderless",
      type: { name: "boolean", required: false },
      description: "Use 'borderless' design for the field",
      control: "boolean",
      table: { category: "Style" },
    },
    standout: {
      title: "standout",
      type: { name: "boolean", required: false },
      description:
        "Use 'standout' design for the field; Specifies classes to be applied when focused (overriding default ones)",
      control: "boolean",
      table: { category: "Style" },
    },
    "hide-bottom-space": {
      title: "hide-bottom-space",
      type: { name: "boolean", required: false },
      description:
        "Do not reserve space for hint/error/counter anymore when these are not used; As a result, it also disables the animation for those; It also allows the hint/error area to stretch vertically based on its content",
      control: "boolean",
      table: { category: "Style" },
    },
    rounded: {
      title: "rounded",
      type: { name: "boolean", required: false },
      description:
        "Applies a small standard border-radius for a squared shape of the component",
      control: "boolean",
      table: { category: "Style" },
    },
    square: {
      title: "square",
      type: { name: "boolean", required: false },
      description:
        "Remove border-radius so borders are squared; Overrides 'rounded' prop",
      control: "boolean",
      table: { category: "Style" },
    },
    dense: {
      title: "dense",
      type: { name: "boolean", required: false },
      description: "Dense mode; occupies less space",
      control: "boolean",
      table: { category: "Style" },
    },
    "item-aligned": {
      title: "item-aligned",
      type: { name: "boolean", required: false },
      description: "Match inner content alignment to that of QItem",
      control: "boolean",
      table: { category: "Style" },
    },
    "input-class": {
      title: "input-class",
      type: { name: "object", required: false },
      description:
        "Class definitions to be attributed to the underlying selection container",
      control: "object",
      table: { category: "Style" },
    },
    "input-style": {
      title: "input-style",
      type: { name: "object", required: false },
      description:
        "Style definitions to be attributed to the underlying selection container",
      control: "object",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "QFile is a component which handles the user interaction for picking file(s).",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QFilePicker },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-file v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  label: "Standard",
};
