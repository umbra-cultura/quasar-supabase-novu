import { QItem } from "quasar";

export default {
  title: "Quasar/Item",
  component: QItem,
  tags: ["autodocs"],
  argTypes: {
    "inset-level": {
      title: "inset-level",
      type: { name: "number", required: false },
      description:
        "Apply an inset; Useful when avatar/left side is missing but you want to align content with other items that do have a left side, or when you're building a menu",
      control: "number",
      table: { category: "Content" },
    },
    tag: {
      title: "tag",
      type: { name: "string", required: false },
      description:
        "HTML tag to render; Suggestion: use 'label' when encapsulating a QCheckbox/QRadio/QToggle so that when user clicks/taps on the whole item it will trigger a model change for the mentioned components",
      defaultValue: "div",
      control: "select",
      options: ["div", "span", "a", "label"],
    },
    tabindex: {
      title: "tabindex",
      type: { name: "number", required: false },
      description: "Tabindex HTML attribute value",
      control: "number",
      table: { category: "General" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    active: {
      title: "active",
      type: { name: "boolean", required: false },
      description: "Put item into 'active' state",
      control: "boolean",
      table: { category: "State" },
    },
    clickable: {
      title: "clickable",
      type: { name: "boolean", required: false },
      description:
        "Is QItem clickable? If it's the case, then it will add hover effects and emit 'click' events",
      control: "boolean",
      table: { category: "State" },
    },
    "manual-focus": {
      title: "manual-focus",
      type: { name: "boolean", required: false },
      description:
        "Put item into a manual focus state; Enables 'focused' prop which will determine if item is focused or not, rather than relying on native hover/focus states",
      control: "boolean",
      table: { category: "State" },
    },
    focused: {
      title: "focused",
      type: { name: "boolean", required: false },
      description:
        "Determines focus state, ONLY if 'manual-focus' is enabled / set to true",
      control: "boolean",
      table: { category: "State" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    dense: {
      title: "dense",
      type: { name: "boolean", required: false },
      description: "Dense mode; occupies less space",
      control: "boolean",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QList and QItem are a group of components which can work together to present multiple line items vertically as a single continuous element. They are best suited for displaying similar data types as rows of information, such as a contact list, a playlist or menu. Each row is called an Item. QItem can also be used outside of a QList too.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QItem },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template:
    '<q-item v-bind="args"><q-item-section><q-item-label header>Item Header</q-item-label><q-item-label caption>Item Caption</q-item-label></q-item-section></q-item>',
});
export const Standard = Template.bind({});
Standard.args = {
  clickable: true,
};
