import { QItemLabel } from "quasar";

export default {
  title: "Quasar/Item Label",
  component: QItemLabel,
  tags: ["autodocs"],
  argTypes: {
    lines: {
      title: "lines",
      type: { name: "number", required: false },
      description:
        "Apply ellipsis when there's not enough space to render on the specified number of lines;",
      control: "number",
      table: { category: "Behavior" },
    },
    overline: {
      title: "overline",
      type: { name: "boolean", required: false },
      description: "Renders an overline label",
      control: "boolean",
      table: { category: "Content" },
    },
    caption: {
      title: "caption",
      type: { name: "boolean", required: false },
      description: "Renders a caption label",
      control: "boolean",
      table: { category: "Content" },
    },
    header: {
      title: "header",
      type: { name: "boolean", required: false },
      description: "Renders a header label",
      control: "boolean",
      table: { category: "Content" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "An item label is useful for predefined text content type within a QItemSection, or for header-like content of the QList itself",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QItemLabel },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-item-label v-bind="args">Item Label</q-item-label>',
});
export const Standard = Template.bind({});
