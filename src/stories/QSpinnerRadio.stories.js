import QSpinnerRadio from "quasar";

export default {
  title: "Quasar/Spinner Radio",
  component: QSpinnerRadio,
  tags: ["autodocs"],
  argTypes: {
    size: {
      title: "size",
      type: { name: "string", required: false },
      description:
        "Size in CSS units, including unit name or standard size name (xs|sm|md|lg|xl)",
      control: "text",
      table: { category: "Style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "A Spinner is used to show the user a timely process is currently taking place. It is an important UX feature, which gives the user the feeling the system is continuing to work for longer term activities, like grabbing data from the server or some heavy calculations.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QSpinnerRadio },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-spinner-radio v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  color: "primary",
  size: "xl",
};
