import QParallax from "quasar";

export default {
  title: "Quasar/Parallax",
  component: QParallax,
  tags: ["autodocs"],
  argTypes: {
    speed: {
      title: "speed",
      type: { name: "number", required: false },
      description: "Speed of parallax effect (0.0 < x < 1.0)",
      control: "number",
      table: { category: "Behavior" },
    },
    "scroll-target": {
      title: "scroll-target",
      type: { name: "string", required: false },
      description:
        "CSS selector or DOM element to be used as a custom scroll container instead of the auto detected one",
      control: "text",
      table: { category: "Behavior" },
    },
    src: {
      title: "src",
      type: { name: "string", required: false },
      description: "Path to image (unless a 'media' slot is used)",
      control: "text",
      table: { category: "Model" },
    },
    height: {
      title: "height",
      type: { name: "number", required: false },
      description: "Height of component (in pixels)",
      defaultValue: 500,
      control: "number",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "Parallax scrolling is a technique in computer graphics and web design, where background images move by the camera slower than foreground images, creating an illusion of depth in a 2D scene and adding to the immersion.\n\nQParallax takes care of a lot of quirks, including image/video size which can actually be smaller than the window width/height.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QParallax },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-parallax v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  src: "https://cdn.quasar.dev/img/parallax2.jpg",
};
