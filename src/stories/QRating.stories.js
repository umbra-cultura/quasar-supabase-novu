import QRating from "quasar";

export default {
  title: "Quasar/Rating",
  component: QRating,
  tags: ["autodocs"],
  argTypes: {
    "icon-aria-label": {
      title: "icon-aria-label",
      type: { name: "object", required: false },
      description:
        "Label to be set on aria-label for Icon; If an array is provided each rating value will use the corresponding aria-label in the array (0 based); If string value is provided the rating value will be appended; If not provided the name of the icon will be used",
      control: "object",
      table: { category: "Accessibility" },
    },
    name: {
      title: "name",
      type: { name: "string", required: false },
      description:
        "Used to specify the name of the control; Useful if dealing with forms submitted directly to a URL",
      control: "object",
      table: { category: "Behavior" },
    },
    icon: {
      title: "icon",
      type: { name: "object", required: false },
      description:
        "Icon name following Quasar convention; make sure you have the icon library installed unless you are using 'img:' prefix; If an array is provided each rating value will use the corresponding icon in the array (0 based)",
      control: "object",
      table: { category: "Content" },
    },
    "icon-selected": {
      title: "icon-selected",
      type: { name: "object", required: false },
      description:
        "Icon name following Quasar convention to be used when selected (optional); make sure you have the icon library installed unless you are using 'img:' prefix; If an array is provided each rating value will use the corresponding icon in the array (0 based)",
      control: "object",
      table: { category: "Content" },
    },
    "icon-half": {
      title: "icon-half",
      type: { name: "object", required: false },
      description:
        "Icon name following Quasar convention to be used when selected (optional); make sure you have the icon library installed unless you are using 'img:' prefix; If an array is provided each rating value will use the corresponding icon in the array (0 based)",
      control: "object",
      table: { category: "Content" },
    },
    max: {
      title: "max",
      type: { name: "number", required: false },
      description: "Number of icons to display",
      defaultValue: 5,
      control: "object",
      table: { category: "General" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "number", required: true },
      description:
        "Model of the component; Either use this property (along with a listener for 'update:model-value' event) OR use v-model directive",
      control: "number",
      table: { category: "Model" },
    },
    "no-reset": {
      title: "no-reset",
      type: { name: "boolean", required: false },
      description:
        "When used, disables default behavior of clicking/tapping on icon which represents current model value to reset model to 0",
      control: "boolean",
      table: { category: "Model" },
    },
    readonly: {
      title: "readonly",
      type: { name: "boolean", required: false },
      description: "Put component in readonly mode",
      control: "boolean",
      table: { category: "State" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    size: {
      title: "size",
      type: { name: "string", required: false },
      description:
        "Size in CSS units, including unit name or standard size name (xs|sm|md|lg|xl)",
      control: "text",
      table: { category: "Style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description:
        "Color name for component from the Quasar Color Palette; v1.5.0+: If an array is provided each rating value will use the corresponding color in the array (0 based)",
      control: "text",
      table: { category: "Style" },
    },
    "color-selected": {
      title: "color-selected",
      type: { name: "string", required: false },
      description: "Color name from the Quasar Palette for selected icons",
      control: "text",
      table: { category: "Style" },
    },
    "color-half": {
      title: "color-half",
      type: { name: "string", required: false },
      description: "Color name from the Quasar Palette for half selected icons",
      control: "text",
      table: { category: "Style" },
    },
    "no-dimming": {
      title: "no-dimming",
      type: { name: "boolean", required: false },
      description: "Does not lower opacity for unselected icons",
      control: "boolean",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "Quasar Rating is a Component which allows users to rate items, usually known as “Star Rating”.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QRating },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-rating v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  "model-value": 3,
};
