import { QItemSection } from "quasar";

export default {
  title: "Quasar/Item Section",
  component: QItemSection,
  tags: ["autodocs"],
  argTypes: {
    avatar: {
      title: "avatar",
      type: { name: "boolean", required: false },
      description:
        "Render an avatar item side (does not needs 'side' prop to be set)",
      control: "boolean",
      table: { category: "Content" },
    },
    thumbnail: {
      title: "thumbnail",
      type: { name: "boolean", required: false },
      description:
        "Render a thumbnail item side (does not needs 'side' prop to be set)",
      control: "boolean",
      table: { category: "Content" },
    },
    side: {
      title: "side",
      type: { name: "boolean", required: false },
      description: "Renders as a side of the item",
      control: "boolean",
      table: { category: "Content" },
    },
    top: {
      title: "top",
      type: { name: "boolean", required: false },
      description: "Align content to top (useful for multi-line items)",
      control: "boolean",
      table: { category: "Content" },
    },
    "no-wrap": {
      title: "no-wrap",
      type: { name: "boolean", required: false },
      description: "Do not wrap text (useful for item's main content)",
      control: "boolean",
      table: { category: "Content" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "An item section can have several uses for particular content. They are controlled via the avatar, thumbnail and side props. With no props, it will render the main section of your QItem (which spans to the fullest of available space).",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QItemSection },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template:
    '<q-item-section v-bind="args"><q-item-label header>Item Header</q-item-label><q-item-label caption>Item Caption</q-item-label></q-item-section>',
});
export const Standard = Template.bind({});
