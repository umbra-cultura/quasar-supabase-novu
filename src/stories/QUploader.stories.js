import QUploader from "quasar";

export default {
  title: "Quasar/Uploader",
  component: QUploader,
  tags: ["autodocs"],
  argTypes: {
    multiple: {
      title: "multiple",
      type: { name: "boolean", required: false },
      description: "Allow multiple file uploads",
      control: "boolean",
      table: { category: "Behavior" },
    },
    accept: {
      title: "accept",
      type: { name: "string", required: false },
      description:
        "Comma separated list of unique file type specifiers. Maps to 'accept' attribute of native input type=file element",
      control: "text",
      table: { category: "Behavior" },
    },
    capture: {
      title: "capture",
      type: { name: "string", required: false },
      description:
        "Optionally, specify that a new file should be captured, and which device should be used to capture that new media of a type defined by the 'accept' prop. Maps to 'capture' attribute of native input type=file element",
      control: "select",
      options: ["user", "environment"],
      table: { category: "Behavior" },
    },
    "max-file-size": {
      title: "max-file-size",
      type: { name: "number", requred: false },
      description: "Maximum size of individual file in bytes",
      control: "number",
      table: { category: "Behavior" },
    },
    "max-total-size": {
      title: "max-total-size",
      type: { name: "number", requred: false },
      description: "Maximum size of all files combined in bytes",
      control: "number",
      table: { category: "Behavior" },
    },
    "max-files": {
      title: "max-files",
      type: { name: "number", requred: false },
      description: "Maximum number of files to contain",
      control: "number",
      table: { category: "Behavior" },
    },
    /*
filter
: (files) => Array
Description
Custom filter for added files; Only files that pass this filter will be added to the queue and uploaded; For best performance, reference it from your scope and do not define it inline
Params
files
: FileList | Array
Description
Candidate files to be added to queue
Return type: Array
Description
Filtered files to be added to queue
Example
:filter="files => files.filter(file => file.size === 1024)"
*/
    "auto-upload": {
      title: "auto-upload",
      type: { name: "boolean", required: false },
      description: "Upload files immediately when added",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "hide-upload-btn": {
      title: "hide-upload-btn",
      type: { name: "boolean", required: false },
      description: "Don't show the upload button",
      control: "boolean",
      table: { category: "Behavior" },
    },
    label: {
      title: "label",
      type: { name: "string", required: false },
      description: "Label for the uploader",
      control: "text",
      table: { category: "Content" },
    },
    "no-thumbnails": {
      title: "no-thumbnails",
      type: { name: "boolean", required: false },
      description: "Don't display thumbnails for image files",
      control: "boolean",
      table: { category: "Content" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    readonly: {
      title: "readonly",
      type: { name: "boolean", required: false },
      description: "Put component in readonly mode",
      control: "boolean",
      table: { category: "State" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    "text-color": {
      title: "text-color",
      type: { name: "string", required: false },
      description:
        "Overrides text color (if needed); Color name from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    square: {
      title: "square",
      type: { name: "boolean", required: false },
      description: "Removes border-radius so borders are squared",
      control: "boolean",
      table: { category: "Style" },
    },
    flat: {
      title: "flat",
      type: { name: "boolean", required: false },
      description: "Applies a 'flat' design (no default shadow)",
      control: "boolean",
      table: { category: "Style" },
    },
    bordered: {
      title: "bordered",
      type: { name: "boolean", required: false },
      description: "Applies a default border to the component",
      control: "boolean",
      table: { category: "Style" },
    },
    url: {
      title: "url",
      type: { name: "string", required: true },
      description:
        "URL or path to the server which handles the upload. Takes String or factory function, which returns String. Function is called right before upload; If using a function then for best performance, reference it from your scope and do not define it inline",
      control: "text",
      table: { category: "Upload" },
    },
    method: {
      title: "method",
      type: { name: "string", required: false },
      description:
        "HTTP method to use for upload; Takes String or factory function which returns a String; Function is called right before upload; If using a function then for best performance, reference it from your scope and do not define it inline",
      defaultValue: "POST",
      control: "select",
      options: ["POST", "PUT"],
      table: { category: "Upload" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "Quasar supplies a way for you to upload files through the QUploader component.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QUploader },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-uploader v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  url: "http://localhost:4444/upload",
  color: "teal",
  flat: true,
  bordered: true,
  style: "max-width: 300px",
};
