import { QMarkupTable } from "quasar";

export default {
  title: "Quasar/Markup Table",
  component: QMarkupTable,
  tags: ["autodocs"],
  argTypes: {
    separator: {
      title: "separator",
      type: { name: "string", required: false },
      description: "Use a separator/border between rows, columns or all cells",
      defaultValue: "horizontal",
      control: "select",
      options: ["horizontal", "vertical", "cell", "none"],
      table: { category: "Content" },
    },
    "wrap-cells": {
      title: "wrap-cells",
      type: { name: "boolean", required: false },
      description: "Wrap text within table cells",
      control: "boolean",
      table: { category: "Content" },
    },
    dense: {
      title: "dense",
      type: { name: "boolean", required: false },
      description: "Dense mode; occupies less space",
      control: "boolean",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    flat: {
      title: "flat",
      type: { name: "boolean", required: false },
      description: "Applies a 'flat' design (no default shadow)",
      control: "boolean",
      table: { category: "Style" },
    },
    bordered: {
      title: "bordered",
      type: { name: "boolean", required: false },
      description: "Applies a default border to the component",
      control: "boolean",
      table: { category: "Style" },
    },
    square: {
      title: "square",
      type: { name: "boolean", required: false },
      description: "Removes border-radius so borders are squared",
      control: "boolean",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QMarkupTable is a way for you to simply wrap a native 'table' in order to make it look like a Material Design table.",
      },
    },
  },
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QMarkupTable },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <q-markup-table v-bind="args">
        <thead>
            <tr>
                <th class="text-left">Dessert (100g serving)</th>
                <th class="text-right">Calories</th>
                <th class="text-right">Fat (g)</th>
                <th class="text-right">Carbs (g)</th>
                <th class="text-right">Protein (g)</th>
                <th class="text-right">Sodium (mg)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left">Frozen Yogurt</td>
                <td class="text-right">159</td>
                <td class="text-right">6</td>
                <td class="text-right">24</td>
                <td class="text-right">4</td>
                <td class="text-right">87</td>
            </tr>
            <tr>
                <td class="text-left">Ice cream sandwich</td>
                <td class="text-right">237</td>
                <td class="text-right">9</td>
                <td class="text-right">37</td>
                <td class="text-right">4.3</td>
                <td class="text-right">129</td>
            </tr>
            <tr>
                <td class="text-left">Eclair</td>
                <td class="text-right">262</td>
                <td class="text-right">16</td>
                <td class="text-right">23</td>
                <td class="text-right">6</td>
                <td class="text-right">337</td>
            </tr>
            <tr>
                <td class="text-left">Cupcake</td>
                <td class="text-right">305</td>
                <td class="text-right">3.7</td>
                <td class="text-right">67</td>
                <td class="text-right">4.3</td>
                <td class="text-right">413</td>
            </tr>
            <tr>
                <td class="text-left">Gingerbread</td>
                <td class="text-right">356</td>
                <td class="text-right">16</td>
                <td class="text-right">49</td>
                <td class="text-right">3.9</td>
                <td class="text-right">327</td>
            </tr>
        </tbody>
    </q-markup-table>
    `,
});
export const Standard = Template.bind({});
