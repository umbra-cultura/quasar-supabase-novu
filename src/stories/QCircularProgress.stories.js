import { QCircularProgress } from "quasar";

export default {
  title: "Quasar/CircularProgress",
  component: QCircularProgress,
  tags: ["autodocs"],
  argTypes: {
    indeterminate: {
      title: "indeterminate",
      type: { name: "boolean", required: false },
      description:
        "Put component into 'indeterminate' state; Ignores 'value' prop",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "show-value": {
      title: "show-value",
      type: { name: "boolean", required: false },
      description:
        "Enables the default slot and uses it (if available), otherwise it displays the 'value' prop as text; Make sure the text has enough space to be displayed inside the component",
      control: "boolean",
      table: { category: "Behavior" },
    },
    reverse: {
      title: "reverse",
      type: { name: "boolean", required: false },
      description:
        "Reverses the direction of progress; Only for determined state",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "instant-feedback": {
      title: "instant-feedback",
      type: { name: "boolean", required: false },
      description: "No animation when model changes",
      control: "boolean",
      table: { category: "Behavior" },
    },
    angle: {
      title: "angle",
      type: { name: "number", required: false },
      description: "Angle to rotate progress arc by",
      defaultValue: 0,
      control: "number",
      table: { category: "Content" },
    },
    value: {
      title: "value",
      type: { name: "number", required: true },
      description: "Current progress (must be between min/max)",
      defaultValue: 0,
      control: "number",
      table: { category: "Model" },
    },
    min: {
      title: "min",
      type: { name: "number", required: false },
      description:
        "Minimum value defining 'no progress' (must be lower than 'max')",
      defaultValue: 0,
      control: "number",
      table: { category: "Model" },
    },
    max: {
      title: "max",
      type: { name: "number", required: false },
      description:
        "Maximum value defining 100% progress made (must be higher than 'min')",
      defaultValue: 100,
      control: "number",
      table: { category: "Model" },
    },
    size: {
      title: "size",
      type: { name: "string", required: false },
      description:
        "Size in CSS units, including unit name or standard size name (xs|sm|md|lg|xl)",
      defaultValue: "md",
      control: "select",
      options: ["xs", "sm", "md", "lg", "xl"],
      table: { category: "style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description:
        "Color name for the arc progress from the Quasar Color Palette",
      control: "text",
      table: { category: "style" },
    },
    "center-color": {
      title: "center-color",
      type: { name: "string", required: false },
      description:
        "Color name for the center part of the component from the Quasar Color Palette",
      control: "text",
      table: { category: "style" },
    },
    "track-color": {
      title: "track-color",
      type: { name: "string", required: false },
      description:
        "Color name for the track of the component from the Quasar Color Palette",
      control: "text",
      table: { category: "style" },
    },
    "font-size": {
      title: "font-size",
      type: { name: "string", required: false },
      description:
        "Size of text in CSS units, including unit name. Suggestion: use 'em' units to sync with component size",
      defaultValue: "0.25em",
      control: "text",
      table: { category: "style" },
    },
    rounded: {
      title: "rounded",
      type: { name: "boolean", required: false },
      description: "Rounding the arc of progress",
      control: "boolean",
      table: { category: "style" },
    },
    thickness: {
      title: "thickness",
      type: { name: "number", required: false },
      description:
        "Thickness of progress arc as a ratio (0.0 < x < 1.0) of component size",
      defaultValue: 0.2,
      control: "number",
      table: { category: "style" },
    },
    "animation-speed": {
      title: "animation-speed",
      type: { name: "number", required: false },
      description: "Animation speed (in milliseconds, without unit)",
      defaultValue: 600,
      control: "number",
      table: { category: "style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QCircularProgress component displays a colored circular progress. The bar can either have a determinate progress, or an indeterminate animation. It should be used to inform the user that an action is occurring in the background.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QCircularProgress },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-circular-progress v-bind="args"/>',
});
export const Standard = Template.bind({});
export const Standard2 = Template.bind({});
export const Standard3 = Template.bind({});
export const Standard4 = Template.bind({});
export const Standard5 = Template.bind({});
export const Standard6 = Template.bind({});

Standard.args = {
  value: 71,
  size: "50px",
  color: "orange",
  class: "q-ma-md",
};
Standard2.args = {
  value: 71,
  size: "90px",
  thickness: 0.2,
  color: "orange",
  "center-color": "grey-8",
  "track-color": "transparent",
  class: "q-ma-md",
};
Standard3.args = {
  value: 71,
  size: "45px",
  thickness: 1,
  color: "grey-8",
  "track-color": "orange",
  class: "q-ma-md",
};
Standard4.args = {
  value: 71,
  size: "50px",
  thickness: 0.22,
  color: "orange",
  "track-color": "grey-3",
  class: "q-ma-md",
};
Standard5.args = {
  value: 71,
  size: "75px",
  thickness: 0.6,
  color: "orange",
  "center-color": "grey-8",
  class: "q-ma-md",
};
Standard6.args = {
  value: 71,
  size: "40px",
  thickness: 0.4,
  color: "orange",
  "track-color": "grey-3",
  "center-color": "grey-8",
  class: "q-ma-md",
};
