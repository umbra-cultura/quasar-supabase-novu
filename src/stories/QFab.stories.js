import { action } from "@storybook/addon-actions";
import { QFab } from "quasar";

export default {
  title: "Quasar/Floating Action Button",
  component: QFab,
  tags: ["autodocs"],
  argTypes: {
    direction: {
      title: "direction",
      type: { name: "string", required: false },
      description: "Direction to expand Fab Actions to",
      control: "text",
      table: { category: "Behavior" },
    },
    persistent: {
      title: "persistent",
      type: { name: "boolean", required: false },
      description:
        "By default, Fab Actions are hidden when user navigates to another route and this prop disables this behavior",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "external-label": {
      title: "external-label",
      type: { name: "boolean", required: false },
      description: "Display label besides the FABs, as external content",
      control: "boolean",
      table: { category: "Content" },
    },
    label: {
      title: "label",
      type: { name: "string", required: false },
      description: "The label that will be shown when Fab is extended",
      control: "text",
      table: { category: "Content" },
    },
    "label-position": {
      title: "label-position",
      type: { name: "string", required: false },
      description: "Position of the label around the icon",
      control: "select",
      options: ["top", "right", "bottom", "left"],
      table: { category: "Content" },
    },
    "hide-label": {
      title: "hide-label",
      type: { name: "boolean", required: false },
      description:
        "Hide the label; Useful for animation purposes where you toggle the visibility of the label",
      control: "boolean",
      table: { category: "Content" },
    },
    icon: {
      title: "icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Content" },
    },
    "active-icon": {
      title: "active-icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Content" },
    },
    "hide-icon": {
      title: "hide-icon",
      type: { name: "boolean", required: false },
      description: "Hide the icon (don't use any)",
      control: "boolean",
      table: { category: "Content" },
    },
    "vertical-actions-align": {
      title: "vertical-actions-align",
      type: { name: "string", required: false },
      description:
        "The side of the Fab where Fab Actions will expand (only when direction is 'up' or 'down')",
      defaultValue: "center",
      control: "select",
      options: ["left", "center", "right"],
      table: { category: "Content" },
    },
    type: {
      title: "type",
      type: { name: "string", required: false },
      description: "Define the button HTML DOM type",
      control: "text",
      table: { category: "General" },
    },
    tabindex: {
      title: "tabindex",
      type: { name: "number", required: false },
      description: "Tabindex HTML attribute value",
      control: "number",
      table: { category: "General" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "boolean", required: false },
      description:
        "Controls state of fab actions (showing/hidden); Works best with v-model directive, otherwise use along listening to 'update:modelValue' event",
      control: "boolean",
      table: { category: "Model" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    outline: {
      title: "outline",
      type: { name: "boolean", required: false },
      description: "Use 'outline' design for Fab button",
      control: "boolean",
      table: { category: "Style" },
    },
    push: {
      title: "push",
      type: { name: "boolean", required: false },
      description: "Use 'push' design for Fab button",
      control: "boolean",
      table: { category: "Style" },
    },
    flat: {
      title: "flat",
      type: { name: "boolean", required: false },
      description: "Use 'flat' design for Fab button",
      control: "boolean",
      table: { category: "Style" },
    },
    unelevated: {
      title: "unelevated",
      type: { name: "boolean", required: false },
      description: "Remove shadow",
      control: "boolean",
      table: { category: "Style" },
    },
    padding: {
      title: "padding",
      type: { name: "string", required: false },
      description:
        "Apply custom padding (vertical [horizontal]); Size in CSS units, including unit name or standard size name (none|xs|sm|md|lg|xl); Also removes the min width and height when set",
      control: "text",
      table: { category: "Style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    "text-color": {
      title: "text-color",
      type: { name: "string", required: false },
      description:
        "Overrides text color (if needed); Color name from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    glossy: {
      title: "glossy",
      type: { name: "boolean", required: false },
      description: "Apply the glossy effect over the button",
      control: "boolean",
      table: { category: "Style" },
    },
    "external-label": {
      title: "external-label",
      type: { name: "boolean", required: false },
      description: "Display label besides the FABs, as external content",
      control: "boolean",
      table: { category: "Style" },
    },
    "label-position": {
      title: "label-position",
      type: { name: "string", required: false },
      description: "Position of the label around the icon",
      control: "select",
      options: ["top", "right", "bottom", "left"],
      table: { category: "Style" },
    },
    "hide-label": {
      title: "hide-label",
      type: { name: "boolean", required: false },
      description:
        "Hide the label; Useful for animation purposes where you toggle the visibility of the label",
      control: "boolean",
      table: { category: "Style" },
    },
    "label-class": {
      title: "label-class",
      type: { name: "object", required: false },
      description: "Class definitions to be attributed to the label container",
      control: "object",
      table: { category: "Style" },
    },
    "label-style": {
      title: "label-style",
      type: { name: "object", required: false },
      description: "Style definitions to be attributed to the label container",
      control: "object",
      table: { category: "Style" },
    },
    square: {
      title: "square",
      type: { name: "boolean", required: false },
      description: "Apply a rectangle aspect to the FAB",
      control: "boolean",
      table: { category: "Style" },
    },
    "hide-icon": {
      title: "hide-icon",
      type: { name: "boolean", required: false },
      description: "Hide the icon (don't use any)",
      control: "boolean",
      table: { category: "Style" },
    },
    "vertical-actions-align": {
      title: "vertical-actions-align",
      type: { name: "string", required: false },
      description:
        "The side of the Fab where Fab Actions will expand (only when direction is 'up' or 'down')",
      defaultValue: "center",
      control: "select",
      options: ["left", "center", "right"],
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "A Floating Action Button (FAB) represents the primary action in a Page. But, it’s not limited to only a single action. It can contain any number of sub-actions too. And more importantly, it can also be used inline in your Pages or Layouts.\n\nNote that you don’t need a QLayout to use FABs.",
      },
    },
  },
};
const Template = (args) => ({
  components: { QFab },
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-fab v-bind="args" @click="click"/>',
  methods: {
    click: action("change!"),
  },
});
export const Standard = Template.bind({});
Standard.args = {
  icon: "add",
  color: "primary",
};
