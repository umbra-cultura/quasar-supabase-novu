import { QLinearProgress } from "quasar";

export default {
  title: "Quasar/Linear Progress",
  component: QLinearProgress,
  tags: ["autodocs"],
  argTypes: {
    buffer: {
      title: "buffer",
      type: { name: "number", required: false },
      description: "Optional buffer value (0.0 < x < 1.0)",
      control: "number",
      table: { category: "Behavior" },
    },
    reverse: {
      title: "reverse",
      type: { name: "boolean", required: false },
      description: "Reverse direction of progress",
      control: "boolean",
      table: { category: "Behavior" },
    },
    indeterminate: {
      title: "indeterminate",
      type: { name: "boolean", required: false },
      description: "Put component into indeterminate mode",
      control: "boolean",
      table: { category: "Behavior" },
    },
    query: {
      title: "query",
      type: { name: "boolean", required: false },
      description: "Put component into query mode",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "instant-feedback": {
      title: "instant-feedback",
      type: { name: "boolean", required: false },
      description: "No transition when model changes",
      control: "boolean",
      table: { category: "Behavior" },
    },
    stripe: {
      title: "stripe",
      type: { name: "boolean", required: false },
      description:
        "Draw stripes; For determinate state only (for performance reasons)",
      control: "boolean",
      table: { category: "Content" },
    },
    value: {
      title: "value",
      type: { name: "number", required: false },
      description: "Progress value (0.0 < x < 1.0)",
      defaultValue: 0,
      control: "number",
      table: { category: "Model" },
    },
    size: {
      title: "size",
      type: { name: "string", required: false },
      description:
        "Size in CSS units, including unit name or standard size name (xs|sm|md|lg|xl)",
      control: "text",
      table: { category: "Style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    "track-color": {
      title: "track-color",
      type: { name: "string", required: false },
      description:
        "Color name for component's track from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    rounded: {
      title: "rounded",
      type: { name: "boolean", required: false },
      description:
        "Applies a small standard border-radius for a squared shape of the component",
      control: "boolean",
      table: { category: "Style" },
    },
    "animation-speed": {
      title: "animation-speed",
      type: { name: "number", required: false },
      description: "Animation speed (in milliseconds, without unit)",
      defaultValue: 2100,
      control: "number",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QLinearProgress component displays a colored loading bar. The bar can either have a determinate progress or an indeterminate animation. It should be used to inform the user that an action is occurring in the background.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QLinearProgress },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<div class="q-pa-md"><q-linear-progress v-bind="args" /></div>',
});
export const Standard = Template.bind({});
Standard.args = {
  value: 0.4,
  class: "q-mt-md",
};
