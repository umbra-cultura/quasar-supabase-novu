import { QForm } from "quasar";

export default {
  title: "Quasar/Form/Form",
  component: QForm,
  tags: ["autodocs"],
  argTypes: {
    autofocus: {
      title: "autofocus",
      type: { name: "boolean", required: false },
      description: "Focus first focusable element on initial component render",
      control: "boolean",
    },
    "no-error-focus": {
      title: "no-error-focus",
      type: { name: "boolean", required: false },
      description:
        "Do not try to focus on first component that has a validation error when submitting form",
      control: "boolean",
    },
    "no-reset-focus": {
      title: "no-reset-focus",
      type: { name: "boolean", required: false },
      description: "Do not try to focus on first component when resetting form",
      control: "boolean",
    },
    greedy: {
      title: "greedy",
      type: { name: "boolean", required: false },
      description:
        "Validate all fields in form (by default it stops after finding the first invalid field with synchronous validation)",
      control: "boolean",
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QForm component renders a <form> DOM element and allows you to easily validate child form components (like QInput, QSelect or your QField wrapped components) that have the internal validation (NOT the external one) through rules associated with them.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QForm },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template:
    '<q-form v-bind="args"><q-input label="Form Example" /><q-btn type="submit" color="primary" label="Submit"></q-form>',
});
export const Standard = Template.bind({});
Standard.args = {};
