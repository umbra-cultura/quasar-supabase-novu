import { QList } from "quasar";

export default {
  title: "Quasar/List",
  component: QList,
  tags: ["autodocs"],
  argTypes: {
    separator: {
      title: "separator",
      type: { name: "boolean", required: false },
      description: "Applies a separator between contained items",
      control: "boolean",
      table: { category: "Content" },
    },
    padding: {
      title: "padding",
      type: { name: "boolean", required: false },
      description: "Applies a material design-like padding on top and bottom",
      control: "boolean",
      table: { category: "Content" },
    },
    tag: {
      title: "tag",
      type: { name: "string", required: false },
      description: "HTML tag to use",
      defaultValue: "div",
      control: "select",
      options: ["div", "span", "ul", "ol"],
      table: { category: "Content" },
    },
    bordered: {
      title: "bordered",
      type: { name: "boolean", required: false },
      description: "Applies a default border to the component",
      control: "boolean",
      table: { category: "Style" },
    },
    dense: {
      title: "dense",
      type: { name: "boolean", required: false },
      description: "Dense mode; occupies less space",
      control: "boolean",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QList and QItem are a group of components which can work together to present multiple line items vertically as a single continuous element. They are best suited for displaying similar data types as rows of information, such as a contact list, a playlist or menu. Each row is called an Item. QItem can also be used outside of a QList too.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QList },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
      <q-list v-bind="args">
        <q-item clickable>
            <q-item-section>
                <q-item-label header>Item Header 1</q-item-label>
                <q-item-label caption>Item Caption 1</q-item-label>
            </q-item-section>
        </q-item>
        <q-item clickable>
            <q-item-section>
                <q-item-label header>Item Header 2</q-item-label>
                <q-item-label caption>Item Caption 2</q-item-label>
            </q-item-section>
        </q-item>
        <q-item clickable>
            <q-item-section>
                <q-item-label header>Item Header 3</q-item-label>
                <q-item-label caption>Item Caption 3</q-item-label>
            </q-item-section>
        </q-item>
      </q-list>`,
});
export const Standard = Template.bind({});
