import { QColor } from "quasar";

export default {
  title: "Quasar/ColorPicker",
  component: QColor,
  tags: ["autodocs"],
  argTypes: {
    name: {
      title: "name",
      type: { name: "string", required: false },
      description:
        "Used to specify the name of the control; Useful if dealing with forms submitted directly to a URL",
      control: "text",
      table: { category: "Behavior" },
    },
    "default-view": {
      title: "default-view",
      type: { name: "string", required: false },
      description: "The default view of the picker",
      defaultValue: "spectrum",
      control: "select",
      options: ["spectrum", "tune", "palette"],
      table: { category: "Behavior" },
    },
    palette: {
      title: "palette",
      type: { name: "object", required: false },
      description: "Use a custom palette of colors for the palette tab",
      control: "object",
      table: { category: "Content" },
    },
    "no-header": {
      title: "no-header",
      type: { name: "boolean", required: false },
      description: "Do not render header",
      control: "boolean",
      table: { category: "Content" },
    },
    "no-header-tabs": {
      title: "no-header-tabs",
      type: { name: "boolean", required: false },
      description: "Do not render header tabs (only the input)",
      control: "boolean",
      table: { category: "Content" },
    },
    "no-footer": {
      title: "no-footer",
      type: { name: "boolean", required: false },
      description:
        "Do not render footer; Useful when you want a specific view ('default-view' prop) and don't want the user to be able to switch it",
      control: "boolean",
      table: { category: "Content" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "string", required: true },
      description:
        "Model of the component; Either use this property (along with a listener for 'update:model-value' event) OR use v-model directive",
      control: "text",
      table: { category: "Model" },
    },
    "default-value": {
      title: "default-value",
      type: { name: "string", required: false },
      description: "The default value to show when the model doesn't have one",
      control: "text",
      table: { category: "Model" },
    },
    "format-model": {
      title: "format-model",
      type: { name: "string", required: false },
      description: "Forces a certain model format upon the model",
      defaultValue: "auto",
      control: "select",
      options: ["auto", "hex", "rgb", "hexa", "rgba"],
      table: { category: "Model" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    readonly: {
      title: "readonly",
      type: { name: "boolean", required: false },
      description: "Put component in readonly mode",
      control: "boolean",
      table: { category: "State" },
    },
    square: {
      title: "square",
      type: { name: "boolean", required: false },
      description: "Removes border-radius so borders are squared",
      control: "boolean",
      table: { category: "Style" },
    },
    flat: {
      title: "flat",
      type: { name: "boolean", required: false },
      description: "Applies a 'flat' design (no default shadow)",
      control: "boolean",
      table: { category: "Style" },
    },
    bordered: {
      title: "bordered",
      type: { name: "boolean", required: false },
      description: "Applies a default border to the component",
      control: "boolean",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component: "The QColor component provides a method to input colors.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QColor },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-color v-bind="args"/>',
});
export const Hexadecimal = Template.bind({});
export const HexadecimalAlpha = Template.bind({});
export const RGB = Template.bind({});
export const RGBAlpha = Template.bind({});

Hexadecimal.args = {
  "model-value": "#FF00FF",
};
HexadecimalAlpha.args = {
  "model-value": "#FF00FFCC",
};
RGB.args = {
  "model-value": "rgb(0,0,0)",
};
RGBAlpha.args = {
  "model-value": "rgba(255,0,255,0.8)",
};
