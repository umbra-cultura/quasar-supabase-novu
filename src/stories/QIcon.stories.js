import { QIcon } from "quasar";

export default {
  title: "Quasar/Icon",
  component: QIcon,
  tags: ["autodocs"],
  argTypes: {
    tag: {
      title: "tag",
      type: { name: "string", required: false },
      description:
        "HTML tag to render, unless no icon is supplied or it's an svg icon",
      defaultValue: "i",
      control: "select",
      options: ["div", "span", "div", "i"],
      table: { category: "Content" },
    },
    left: {
      title: "left",
      type: { name: "boolean", required: false },
      description:
        "Useful if icon is on the left side of something: applies a standard margin on the right side of Icon",
      control: "boolean",
      table: { category: "Content" },
    },
    right: {
      title: "right",
      type: { name: "boolean", required: false },
      description:
        "Useful if icon is on the right side of something: applies a standard margin on the left side of Icon",
      control: "boolean",
      table: { category: "Content" },
    },
    name: {
      title: "tag",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Model" },
    },
    size: {
      title: "size",
      type: { name: "string", required: false },
      description:
        "Size in CSS units, including unit name or standard size name (xs|sm|md|lg|xl)",
      control: "text",
      table: { category: "Style" },
    },
    color: {
      title: "color",
      type: { name: "string", required: false },
      description: "Color name for component from the Quasar Color Palette",
      control: "text",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QIcon component allows you to easily insert icons within other components or any other area of your pages.\n\nFurthermore you can add support by yourself for any icon lib.\n\nThere are multiple types of icons in Quasar: webfont-based, svg-based and image-based. You are not bound to using only one type in your website/app.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QIcon },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-icon v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  name: "today",
  class: "text-orange",
  size: "5em",
};
