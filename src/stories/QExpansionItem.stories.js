import { QExpansionItem } from "quasar";

export default {
  title: "Quasar/Expansion Item",
  component: QExpansionItem,
  tags: ["autodocs"],
  argTypes: {
    "toggle-aria-label": {
      title: "toggle-aria-label",
      type: { name: "string", required: false },
      description: "aria-label to be used on the expansion toggle element",
      control: "text",
      table: { category: "Accessibility" },
    },
    duration: {
      title: "duration",
      type: { name: "number", required: false },
      description: "Animation duration (in milliseconds)",
      defaultValue: 300,
      control: "text",
      table: { category: "Behavior" },
    },
    "default-opened": {
      title: "default-opened",
      type: { name: "boolean", required: false },
      description:
        "Puts expansion item into open state on initial render; Overridden by v-model if used",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "expand-icon-toggle": {
      title: "expand-icon-toggle",
      type: { name: "boolean", required: false },
      description:
        "Applies the expansion events to the expand icon only and not to the whole header",
      control: "boolean",
      table: { category: "Behavior" },
    },
    group: {
      title: "group",
      type: { name: "string", required: false },
      description:
        "Register expansion item into a group (unique name that must be applied to all expansion items in that group) for coordinated open/close state within the group a.k.a. 'accordion mode'",
      control: "text",
      table: { category: "Behavior" },
    },
    popup: {
      title: "popup",
      type: { name: "boolean", required: false },
      description: "Put expansion list into 'popup' mode",
      control: "boolean",
      table: { category: "Behavior" },
    },
    icon: {
      title: "icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Content" },
    },
    "expand-icon": {
      title: "expand-icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Content" },
    },
    "expanded-icon": {
      title: "expanded-icon",
      type: { name: "string", required: false },
      description:
        "Expand icon name (following Quasar convention) for when QExpansionItem is expanded; When used, it also disables the rotation animation of the expand icon; Make sure you have the icon library installed unless you are using 'img:' prefix",
      control: "text",
      table: { category: "Content" },
    },
    label: {
      title: "label",
      type: { name: "string", required: false },
      description: "Header label (unless using 'header' slot)",
      control: "text",
      table: { category: "Content" },
    },
    "label-lines": {
      title: "label-lines",
      type: { name: "number", required: false },
      description:
        "Apply ellipsis when there's not enough space to render on the specified number of lines; If more than one line specified, then it will only work on webkit browsers because it uses the '-webkit-line-clamp' CSS property!",
      control: "number",
      table: { category: "Content" },
    },
    caption: {
      title: "caption",
      type: { name: "string", required: false },
      description: "Header sub-label (unless using 'header' slot)",
      control: "text",
      table: { category: "Content" },
    },
    "caption-lines": {
      title: "caption-lines",
      type: { name: "number", required: false },
      description:
        "Apply ellipsis when there's not enough space to render on the specified number of lines; If more than one line specified, then it will only work on webkit browsers because it uses the '-webkit-line-clamp' CSS property!",
      control: "number",
      table: { category: "Content" },
    },
    "header-inset-level": {
      title: "header-inset-level",
      type: { name: "number", required: false },
      description:
        "Apply an inset to header (unless using 'header' slot); Useful when header avatar/left side is missing but you want to align content with other items that do have a left side, or when you're building a menu",
      control: "number",
      table: { category: "Content" },
    },
    "content-inset-level": {
      title: "content-inset-level",
      type: { name: "number", required: false },
      description: "Apply an inset to content (changes content padding)",
      control: "number",
      table: { category: "Content" },
    },
    "expand-separator": {
      title: "expand-separator",
      type: { name: "boolean", required: false },
      description:
        "Apply a top and bottom separator when expansion item is opened",
      control: "boolean",
      table: { category: "Content" },
    },
    "hide-expand-icon": {
      title: "hide-expand-icon",
      type: { name: "boolean", required: false },
      description: "Do not show the expand icon",
      control: "boolean",
      table: { category: "Content" },
    },
    "switch-toggle-side": {
      title: "switch-toggle-side",
      type: { name: "boolean", required: false },
      description: "Switch expand icon side (from default 'right' to 'left')",
      control: "boolean",
      table: { category: "Content" },
    },
    group: {
      title: "group",
      type: { name: "string", required: false },
      description:
        "Register expansion item into a group (unique name that must be applied to all expansion items in that group) for coordinated open/close state within the group a.k.a. 'accordion mode'",
      control: "text",
      table: { category: "Content" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "boolean", required: false },
      description:
        "Model of the component defining 'open' state; Either use this property (along with a listener for 'update:modelValue' event) OR use v-model directive",
      control: "boolean",
      table: { category: "Model" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    "expand-icon-class": {
      title: "expand-icon-class",
      type: { name: "object", required: false },
      description: "Apply custom class(es) to the expand icon item section",
      control: "object",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    dense: {
      title: "dense",
      type: { name: "boolean", required: false },
      description: "Dense mode; occupies less space",
      control: "boolean",
      table: { category: "Style" },
    },
    "dense-toggle": {
      title: "dense-toggle",
      type: { name: "boolean", required: false },
      description: "Use dense mode for expand icon",
      control: "boolean",
      table: { category: "Style" },
    },
    "header-style": {
      title: "header-style",
      type: { name: "object", required: false },
      description: "Apply custom style to the header",
      control: "object",
      table: { category: "Style" },
    },
    "header-class": {
      title: "header-class",
      type: { name: "object", required: false },
      description: "Apply custom class(es) to the header",
      control: "object",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QExpansionItem component allows the hiding of content that is not immediately relevant to the user. Think of them as accordion elements that expand when clicked on. It’s also known as a collapsible.\n\nThey are basically QItem components wrapped with additional functionality. So they can be included in QLists and inherit QItem component properties.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QExpansionItem },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template:
    '<q-expansion-item v-bind="args"><q-card><q-card-section>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, eius reprehenderit eos corrupti commodi magni quaerat ex numquam, dolorum officiis modi facere maiores architecto suscipit iste eveniet doloribus ullam aliquid. </q-card-section></q-card></q-expansion-item>',
});
export const Standard = Template.bind({});
Standard.args = {
  "expand-separator": true,
  icon: "perm_identity",
  label: "Account settings",
  caption: "John Doe",
};
