import { action } from "@storybook/addon-actions";
import { QFabAction } from "quasar";

export default {
  title: "Quasar/Floating Action Button Actions",
  component: QFabAction,
  tags: ["autodocs"],
  argTypes: {
    "external-label": {
      title: "external-label",
      type: { name: "boolean", required: false },
      description: "Display label besides the FABs, as external content",
      control: "boolean",
      table: { category: "Content" },
    },
    label: {
      title: "label",
      type: { name: "string", required: false },
      description: "The label that will be shown when Fab is extended",
      control: "text",
      table: { category: "Content" },
    },
    "label-position": {
      title: "label-position",
      type: { name: "string", required: false },
      description: "Position of the label around the icon",
      control: "select",
      options: ["top", "right", "bottom", "left"],
      table: { category: "Content" },
    },
    "hide-label": {
      title: "hide-label",
      type: { name: "boolean", required: false },
      description:
        "Hide the label; Useful for animation purposes where you toggle the visibility of the label",
      control: "boolean",
      table: { category: "Content" },
    },
    icon: {
      title: "icon",
      type: { name: "string", required: false },
      description:
        "Icon name following Quasar convention; Make sure you have the icon library installed unless you are using 'img:' prefix; If 'none' (String) is used as value then no icon is rendered (but screen real estate will still be used for it)",
      control: "text",
      table: { category: "Content" },
    },
    anchor: {
      title: "anchor",
      type: { name: "string", required: false },
      description:
        "How to align the Fab Action relative to Fab expand side; By default it uses the align specified in QFab",
      control: "select",
      options: ["start", "center", "end"],
      table: { category: "Content" },
    },
    type: {
      title: "type",
      type: { name: "string", required: false },
      description: "Define the button HTML DOM type",
      defaultValue: "a",
      control: "select",
      options: ["a", "submit", "button", "reset"],
      table: { category: "General" },
    },
    tabindex: {
      title: "tabindex",
      type: { name: "number", required: false },
      description: "Tabindex HTML attribute value",
      control: "number",
      table: { category: "Content" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "A Floating Action Button (FAB) represents the primary action in a Page. But, it’s not limited to only a single action. It can contain any number of sub-actions too. And more importantly, it can also be used inline in your Pages or Layouts.\n\nNote that you don’t need a QLayout to use FABs.",
      },
    },
  },
};
const Template = (args) => ({
  components: { QFabAction },
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-fab><q-fab-action v-bind="args" @click="click"/></q-fab>',
  methods: {
    click: action("click!"),
  },
});
export const Standard = Template.bind({});
Standard.args = {
  color: "amber",
  "text-color": "black",
  icon: "alarm",
};
