import { QImg } from "quasar";

export default {
  title: "Quasar/Image",
  component: QImg,
  tags: ["autodocs"],
  argTypes: {
    loading: {
      title: "loading",
      type: { name: "string", required: false },
      description:
        "Lazy or immediate load; Same syntax as <img> loading attribute",
      defaultValue: "lazy",
      control: "select",
      options: ["lazy", "eager"],
      table: { category: "Behavior" },
    },
    crossorigin: {
      title: "crossorigin",
      type: { name: "string", required: false },
      description: "Same syntax as <img> crossorigin attribute",
      control: "select",
      options: ["anonymous", "use-credentials"],
      table: { category: "Behavior" },
    },
    decoding: {
      title: "decoding",
      type: { name: "string", required: false },
      description: "Same syntax as <img> decoding attribute",
      control: "select",
      options: ["sync", "async", "auto"],
      table: { category: "Behavior" },
    },
    referrerpolicy: {
      title: "referrerpolicy",
      type: { name: "string", required: false },
      description: "Same syntax as <img> referrerpolicy attribute",
      control: "select",
      options: [
        "no-referrer",
        "no-referrer-when-downgrade",
        "origin",
        "origin-when-cross-origin",
        "same-origin",
        "strict-origin",
        "strict-origin-when-cross-origin",
        "unsafe-url",
      ],
      table: { category: "Behavior" },
    },
    fetchpriority: {
      title: "fetchpriority",
      type: { name: "string", required: false },
      description:
        "Provides a hint of the relative priority to use when fetching the image",
      defaultValue: "auto",
      control: "select",
      options: ["high", "low", "auto"],
      table: { category: "Behavior" },
    },
    draggable: {
      title: "draggable",
      type: { name: "boolean", required: false },
      description: "Adds the native 'draggable' attribute",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "no-spinner": {
      title: "no-spinner",
      type: { name: "boolean", required: false },
      description:
        "Do not display the default spinner while waiting for the image to be loaded; It is overriden by the 'loading' slot when one is present",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "no-native-menu": {
      title: "no-native-menu",
      type: { name: "boolean", required: false },
      description: "Disables the native context menu for the image",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "no-transition": {
      title: "no-transition",
      type: { name: "boolean", required: false },
      description:
        "Disable default transition when switching between old and new image",
      control: "boolean",
      table: { category: "Behavior" },
    },
    alt: {
      title: "alt",
      type: { name: "string", required: false },
      description:
        "Specifies an alternate text for the image, if the image cannot be displayed",
      control: "text",
      table: { category: "Content" },
    },
    src: {
      title: "src",
      type: { name: "string", required: false },
      description: "Path to image",
      control: "text",
      table: { category: "Model" },
    },
    srcset: {
      title: "srcset",
      type: { name: "string", required: false },
      description: "Same syntax as <img> srcset attribute",
      control: "text",
      table: { category: "Model" },
    },
    sizes: {
      title: "sizes",
      type: { name: "string", required: false },
      description: "Same syntax as <img> sizes attribute",
      control: "text",
      table: { category: "Model" },
    },
    "placeholder-src": {
      title: "placeholder-src",
      type: { name: "string", required: false },
      description:
        "While waiting for your image to load, you can use a placeholder image",
      control: "text",
      table: { category: "Model" },
    },
    ratio: {
      title: "ratio",
      type: { name: "string", required: false },
      description: "Force the component to maintain an aspect ratio",
      control: "text",
      table: { category: "Style" },
    },
    "initial-ratio": {
      title: "initial-ratio",
      type: { name: "string", required: false },
      description:
        "Use it when not specifying 'ratio' but still wanting an initial aspect ratio",
      defaultValue: "16/9",
      control: "text",
      table: { category: "Style" },
    },
    width: {
      title: "width",
      type: { name: "string", required: false },
      description: "Forces image width; Must also include the unit (px or %)",
      control: "text",
      table: { category: "Style" },
    },
    height: {
      title: "height",
      type: { name: "string", required: false },
      description: "Forces image height; Must also include the unit (px or %)",
      control: "text",
      table: { category: "Style" },
    },
    fit: {
      title: "fit",
      type: { name: "string", required: false },
      description:
        "How the image will fit into the container; Equivalent of the object-fit prop; Can be coordinated with 'position' prop",
      defaultValue: "cover",
      control: "select",
      options: ["cover", "fill", "contain", "none", "scale-down"],
      table: { category: "Style" },
    },
    position: {
      title: "fit",
      type: { name: "string", required: false },
      description:
        "The alignment of the image into the container; Equivalent of the object-position CSS prop",
      defaultValue: "50% 50%",
      control: "text",
      table: { category: "Style" },
    },
    "img-class": {
      title: "img-class",
      type: { name: "string", required: false },
      description: "CSS classes to be attributed to the native img element",
      control: "text",
      table: { category: "Style" },
    },
    "img-style": {
      title: "img-style",
      type: { name: "object", required: false },
      description: "Apply CSS to the native img element",
      control: "object",
      table: { category: "Style" },
    },
    "spinner-color": {
      title: "spinner-color",
      type: { name: "string", required: false },
      description:
        "Color name for default Spinner (unless using a 'loading' slot)",
      control: "text",
      table: { category: "Style" },
    },
    "spinner-size": {
      title: "spinner-size",
      type: { name: "string", required: false },
      description:
        "Size in CSS units, including unit name, for default Spinner (unless using a 'loading' slot)",
      control: "text",
      table: { category: "Style" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QImg component makes working with images (any picture format) easy and also adds a nice loading effect to it along with many other features (example: the ability to set an aspect ratio).",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QImg },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-img v-bind="args" />',
});
export const Standard = Template.bind({});
Standard.args = {
  src: "https://picsum.photos/500/300",
  "spinner-color": "white",
  ratio: "16/9",
};
