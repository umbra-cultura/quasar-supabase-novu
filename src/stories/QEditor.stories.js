import { QEditor } from "quasar";

export default {
  title: "Quasar/Editor(WYSIWYG)",
  component: QEditor,
  tags: ["autodocs"],
  argTypes: {
    fullscreen: {
      title: "fullscreen",
      type: { name: "boolean", required: false },
      description: "Fullscreen mode",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "no-route-fullscreen-exit": {
      title: "no-route-fullscreen-exit",
      type: { name: "boolean", required: false },
      description: "Changing route app won't exit fullscreen",
      control: "boolean",
      table: { category: "Behavior" },
    },
    "paragraph-tag": {
      title: "paragraph-tag",
      type: { name: "string", required: false },
      description: "Paragraph tag to be used",
      control: "select",
      options: ["div", "p"],
      table: { category: "Behavior" },
    },
    placeholder: {
      title: "placeholder",
      type: { name: "string", required: false },
      description: "Text to display as placeholder",
      control: "text",
      table: { category: "Content" },
    },
    "model-value": {
      title: "model-value",
      type: { name: "string", required: true },
      description:
        "Model of the component; Either use this property (along with a listener for 'update:modelValue' event) OR use v-model directive",
      control: "text",
      table: { category: "Model" },
    },
    readonly: {
      title: "readonly",
      type: { name: "boolean", required: false },
      description: "Put component in readonly mode",
      control: "boolean",
      table: { category: "State" },
    },
    disable: {
      title: "disable",
      type: { name: "boolean", required: false },
      description: "Put component in disabled mode",
      control: "boolean",
      table: { category: "State" },
    },
    square: {
      title: "square",
      type: { name: "boolean", required: false },
      description: "Removes border-radius so borders are squared",
      control: "boolean",
      table: { category: "Style" },
    },
    flat: {
      title: "flat",
      type: { name: "boolean", required: false },
      description: "Applies a 'flat' design (no borders)",
      control: "boolean",
      table: { category: "Style" },
    },
    dense: {
      title: "dense",
      type: { name: "boolean", required: false },
      description: "Dense mode; toolbar buttons are shown on one-line only",
      control: "boolean",
      table: { category: "Style" },
    },
    dark: {
      title: "dark",
      type: { name: "boolean", required: false },
      description: "Notify the component that the background is a dark color",
      control: "boolean",
      table: { category: "Style" },
    },
    "min-height": {
      title: "min-height",
      type: { name: "string", required: false },
      description: "CSS unit for the minimum height of the editable area",
      defaultValue: "10rem",
      control: "text",
      table: { category: "Style" },
    },
    "max-height": {
      title: "max-height",
      type: { name: "string", required: false },
      description: "CSS unit for maximum height of the input area",
      control: "text",
      table: { category: "Style" },
    },
    height: {
      title: "height",
      type: { name: "string", required: false },
      description: "CSS value to set the height of the editable area",
      control: "text",
      table: { category: "Style" },
    },
    "toolbar-outline": {
      title: "toolbar-outline",
      type: { name: "boolean", required: false },
      description: 'Toolbar buttons are rendered "outlined"',
      control: "boolean",
      table: { category: "Style" },
    },
    "toolbar-push": {
      title: "toolbar-push",
      type: { name: "boolean", required: false },
      description: 'Toolbar buttons are rendered as a "push-button" type',
      control: "boolean",
      table: { category: "Style" },
    },
    "toolbar-rounded": {
      title: "toolbar-rounded",
      type: { name: "boolean", required: false },
      description: 'Toolbar buttons are rendered "rounded"',
      control: "boolean",
      table: { category: "Style" },
    },
    "content-style": {
      title: "content-style",
      type: { name: "object", required: false },
      description:
        "Object with CSS properties and values for styling the container of QEditor",
      control: "object",
      table: { category: "Style" },
    },
    "content-class": {
      title: "content-class",
      type: { name: "object", required: false },
      description: "CSS classes for the input area",
      control: "object",
      table: { category: "Style" },
    },
    definitions: {
      title: "definitions",
      type: { name: "object", required: false },
      description:
        "Definition of commands and their buttons to be included in the 'toolbar' prop",
      control: "object",
      table: { category: "Toolbox" },
    },
    fonts: {
      title: "fonts",
      type: { name: "object", required: false },
      description: "Object with definitions of fonts",
      control: "object",
      table: { category: "Toolbox" },
    },
    toolbar: {
      title: "toolbox",
      type: { name: "object", required: false },
      description:
        "An array of arrays of Objects/Strings that you use to define the construction of the elements and commands available in the toolbar",
      defaultValue: [
        "left",
        "center",
        "right",
        "justify",
        "bold",
        "italic",
        "underline",
        "strike",
        "undo",
        "redo",
      ],
      control: "object",
      table: { category: "Toolbox" },
    },
    "toolbar-color": {
      title: "toolbar-color",
      type: { name: "string", required: false },
      description:
        "Font color (from the Quasar Palette) of buttons and text in the toolbar",
      control: "text",
      table: { category: "Toolbox" },
    },
    "toolbar-text-color": {
      title: "toolbar-text-color",
      type: { name: "string", required: false },
      description: "Text color (from the Quasar Palette) of toolbar commands",
      control: "text",
      table: { category: "Toolbox" },
    },
    "toolbar-toggle-color": {
      title: "toolbar-toggle-color",
      type: { name: "string", required: false },
      description:
        "Choose the active color (from the Quasar Palette) of toolbar commands button",
      defaultValue: "primary",
      control: "text",
      table: { category: "Toolbox" },
    },
    "toolbar-bg": {
      title: "toolbar-bg",
      type: { name: "string", required: false },
      description: "Toolbar background color (from Quasar Palette)",
      defaultValue: "grey-3",
      control: "text",
      table: { category: "Toolbox" },
    },
    "toolbar-outline": {
      title: "toolbar-outline",
      type: { name: "boolean", required: false },
      description: 'Toolbar buttons are rendered "outlined"',
      control: "boolean",
      table: { category: "Toolbox" },
    },
    "toolbar-push": {
      title: "toolbar-push",
      type: { name: "boolean", required: false },
      description: 'Toolbar buttons are rendered as a "push-button" type',
      control: "boolean",
      table: { category: "Toolbox" },
    },
    "toolbar-rounded": {
      title: "toolbar-rounded",
      type: { name: "boolean", required: false },
      description: 'Toolbar buttons are rendered "rounded"',
      control: "boolean",
      table: { category: "Toolbox" },
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component:
          "The QEditor component is a WYSIWYG (“what you see is what you get”) editor component that enables the user to write and even paste HTML. It uses the so-called Design Mode and the cross-browser contentEditable interface.",
      },
    },
  },
};
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { QEditor },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return { args };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<q-editor v-bind="args"/>',
});
export const Standard = Template.bind({});
Standard.args = {
  "model-value": "What you see is what you get...",
  "min-height": "5rem",
  definitions: {
    save: {
      tip: "Save your work",
      icon: "save",
      label: "Save",
      handler: () => {},
    },
    upload: {
      tip: "Upload to cloud",
      icon: "cloud_upload",
      label: "Upload",
      handler: () => {},
    },
  },
  fonts: {
    arial: "Arial",
    arial_black: "Arial Black",
    comic_sans: "Comic Sans MS",
  },
};
